package com.taledir.microservices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LoginBadRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public LoginBadRequestException(String arg0) {
		super(arg0);
	}

}

