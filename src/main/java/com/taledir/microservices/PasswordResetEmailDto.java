package com.taledir.microservices;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class PasswordResetEmailDto {

  @Email(message = "Please enter a valid email address")
  @NotBlank(message = "Email must be specified")
	private String email;

  protected PasswordResetEmailDto() {

	}

  public PasswordResetEmailDto(
    @Email(message = "Please enter a valid email address") @NotBlank(message = "Email must be specified") String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}