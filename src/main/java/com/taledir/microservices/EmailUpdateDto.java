package com.taledir.microservices;

import javax.validation.constraints.Email;

public class EmailUpdateDto {

	private String token;

  @Email(message = "Please enter a valid email address")
	private String email;

	public EmailUpdateDto(String token, String email) {
    this.token = token;
    this.email = email;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}