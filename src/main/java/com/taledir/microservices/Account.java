package com.taledir.microservices;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@NaturalId(mutable = false)
	@Column(name = "username", nullable = false, unique = true)
	private String username;

	@Column(name = "email", nullable = false, unique = true)
	private String email;

	@Column(name = "password")
	@JsonIgnore
	private String password;

	@Column(name = "is_account_locked")
	private Boolean isAccountLocked;

	@Column(name = "is_account_disabled")
	private Boolean isAccountDisabled;

	protected Account() {

	}

	public Account(String username, String email, String password) {
		super();
    this.username = username;
    this.email = email;
		this.password = password;
		this.isAccountLocked = false;
		this.isAccountDisabled = false;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getIsAccountLocked() {
		return isAccountLocked;
	}

	public void setIsAccountLocked(Boolean isAccountLocked) {
		this.isAccountLocked = isAccountLocked;
	}

	public Boolean getIsAccountDisabled() {
		return isAccountDisabled;
	}

	public void setIsAccountDisabled(Boolean isAccountDisabled) {
		this.isAccountDisabled = isAccountDisabled;
	}

}
