package com.taledir.microservices;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "password_reset")
public class PasswordReset {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "email", nullable = false, unique = true)
	private String email;

	@Column(name = "token", nullable = false)
	@JsonIgnore
	private String token;

	@PastOrPresent
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_timestamp", nullable = false)
	private Date createTimestamp;

	protected PasswordReset() {

	}

	public PasswordReset(String email, String token) {
		super();
    this.email = email;
    this.token = token;
    this.createTimestamp = new Date();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

  public Date getCreateTimestamp() {
    return createTimestamp;
  }

  public void setCreateTimestamp(Date createTimestamp) {
    this.createTimestamp = createTimestamp;
  }

}