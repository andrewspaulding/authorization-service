package com.taledir.microservices;

import java.security.KeyPair;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

@Service
public class AuthorizationService {

	private static final int EXPIRE_SECONDS = 900;

	Logger logger = LoggerFactory.getLogger(AuthorizationService.class);

	@Autowired
	AuthorizationKeyStoreRepository authorizationKeyStoreRepository;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	AccountRepository accountRepository;

  @Autowired
  PasswordResetRepository passwordResetRepository;

	@Autowired
  SigningKeyResolver signingKeyResolver;
  
  @Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

  //TODO:  Still need to create expiration date and cleanup cron job

  //TODO:  refactor code so that validateCredentials and update password share same code

  //TODO:  optimize the login query requests

	private Account validateCredentials(Credentials credentials) {
    String email = credentials.getEmail();
    
		if(email == null || credentials.getPassword() == null) {
			throw new LoginBadRequestException("Please fill in email and password");
		}

    Account account = accountRepository.findOneByEmail(email);

		return account;
	}

	protected void createAccount(Credentials credentials) {

		Account usernameAccount = accountRepository.findOneByUsername(credentials.getUsername());

		if(usernameAccount != null) {
			throw new AccountAlreadyExistsException("Username is already in use.");
		}

		Account emailAccount = accountRepository.findOneByEmail(credentials.getEmail());

		if(emailAccount != null) {
			throw new AccountAlreadyExistsException("Email is already in use.");
		}

		String encodedPassword = bCryptPasswordEncoder.encode(credentials.getPassword());

		Account newAccount = new Account(credentials.getUsername(), credentials.getEmail(), encodedPassword);

		accountRepository.save(newAccount);

	}

  private Account getAccountByToken(String token) {
    String username;
		try {
			Jws<Claims> claims = getClaims(token);
			username = claims.getBody().getSubject();
      if(username == null || username.length() == 0) {
        throw new LoginUnauthorizedException("Not authorized to update");
      }
		} catch(LoginBadRequestException e) {
			throw new LoginBadRequestException("Not authorized to update");
		}

		Account account = accountRepository.findOneByUsername(username);

		if(account == null) {
			throw new LoginBadRequestException("Account does not exist, cannot update");
		}

    return account;

  }

	protected Map<String, Object> updatePassword(String token, PasswordUpdate passwordUpdate) {

    Account account = getAccountByToken(token);

    boolean matches = bCryptPasswordEncoder.matches(
			passwordUpdate.getCurrentPassword(), account.getPassword());

		if(!matches) {
			throw new LoginUnauthorizedException("Incorrect existing password.");
		}
    
		String encodedPassword = bCryptPasswordEncoder.encode(passwordUpdate.getNewPassword());

		account.setPassword(encodedPassword);

		accountRepository.save(account);

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("success", true);

		return body;
  }
  
  protected Map<String, Object> updateEmail(String token, String email) {

    Account account = getAccountByToken(token);

    //skip updating if the values are already the same
    if(!email.equals(account.getEmail())) {
      Account emailAccount = accountRepository.findOneByEmail(email);

      if(emailAccount != null) {
        throw new AccountAlreadyExistsException("Email is already in use.");
      }
  
      account.setEmail(email);
      accountRepository.save(account);
    }

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("success", true);

		return body;
	}

	protected String validateExistingCredentials(Credentials credentials) {

		Account account = validateCredentials(credentials);

		if(account == null) {
			throw new LoginUnauthorizedException("Incorrect email or password.");
		}

		boolean matches = bCryptPasswordEncoder.matches(
			credentials.getPassword(), account.getPassword());

		if(!matches) {
			throw new LoginUnauthorizedException("Incorrect email or password.");
    }
    
    //returns username, to handle case where email was used for authentication
    return account.getUsername();

	}

	protected String getLoginToken(String username) {

    //TODO:  username will now be used for requesting removal of all active sessions for a given user
    AuthorizationKeyStore keyStore = new AuthorizationKeyStore(username);
		return buildToken(keyStore, username);

	}

  private UUID getKeyStoreId(AuthorizationKeyStore keyStore, byte[] pubKey, String username) {

    //for brand new entries, create a next UUID
    if(keyStore.getNextId() == null) {
      AuthorizationKeyStore nextKeyStore = authorizationKeyStoreRepository.save(new AuthorizationKeyStore(username));
      keyStore.setNextId(nextKeyStore.getId());
      keyStore.setPubKey(pubKey);
      AuthorizationKeyStore savedKeyStore = authorizationKeyStoreRepository.save(keyStore);
      return savedKeyStore.getId();
    }
    else {
      //for refresh on given UUID, update the next UUID, and assign it a previous UUID
      AuthorizationKeyStore nextKeyStore = authorizationKeyStoreRepository.getOne(keyStore.getNextId());
      nextKeyStore.setPrevId(keyStore.getId());
      nextKeyStore.setPubKey(pubKey);

      if(keyStore.getPrevId() == null) {
        AuthorizationKeyStore nextNextKeyStore = authorizationKeyStoreRepository.save(new AuthorizationKeyStore(username));
        nextKeyStore.setNextId(nextNextKeyStore.getId());
      }
      else {
        //move the previous ID to be next's next ID
        nextKeyStore.setNextId(keyStore.getPrevId());
      }
      AuthorizationKeyStore savedNextKeyStore = authorizationKeyStoreRepository.save(nextKeyStore);
      return savedNextKeyStore.getId();
    }
  }

	private String buildToken(AuthorizationKeyStore keyStore, String username) {
    
    KeyPair keyPair = Keys.keyPairFor(SignatureAlgorithm.RS256);
		byte[] pubKey = keyPair.getPublic().getEncoded();

    UUID kid = getKeyStoreId(keyStore, pubKey, username);

		String jwtToken = Jwts.builder()
			.setHeaderParam(JwsHeader.KEY_ID, kid.toString())
			.setSubject(username)
			.claim("roles", "user")
			.setIssuedAt(new Date())
			.setExpiration(new Date(System.currentTimeMillis() + (EXPIRE_SECONDS * 1000)))
			.signWith(keyPair.getPrivate())
			.compact();

		return jwtToken;
	}

	private Jws<Claims> getClaims(String jwsString) {
		try {
			Jws<Claims> claims = Jwts.parserBuilder()
				.setSigningKeyResolver(signingKeyResolver)
				.build()
				.parseClaimsJws(jwsString);
			
			return claims;
		}
		catch(ExpiredJwtException e) {
			String keyId = ((String) e.getHeader().get("kid"));
			UUID kid = UUID.fromString(keyId);
			authorizationKeyStoreRepository.deleteById(kid);
			throw new LoginBadRequestException("Login session expired");
		} catch(JwtException e) {
			throw new LoginBadRequestException("Invalid login session");
		}
	}

	protected Map<String, Object> getTokenInfoBody(String jwsString) {
		Map<String, Object> body = new HashMap<String, Object>();
		String active = "active";

		if(jwsString == null || jwsString.length() == 0) {
			body.put(active, false);
			return body;
		}

		try {
			Jws<Claims> claims = getClaims(jwsString);

			body.put(active, true);
			body.put("roles", claims.getBody().get("roles"));
			body.put("username", claims.getBody().getSubject());
			body.put("exp", claims.getBody().getExpiration());

		} catch(LoginBadRequestException e) {
			body.put(active, false);
		}
		return body;
	}

	protected void logout(String jwsString) {
		Jws<Claims> claims = getClaims(jwsString);

		String keyId = claims.getHeader().getKeyId();
		UUID kid = UUID.fromString(keyId);

    AuthorizationKeyStore keyStore = authorizationKeyStoreRepository.getOne(kid);
    if(keyStore.getPrevId() != null) {
      authorizationKeyStoreRepository.deleteById(keyStore.getPrevId());
    }
    if(keyStore.getNextId() != null) {
      authorizationKeyStoreRepository.deleteById(keyStore.getNextId());
    }
		authorizationKeyStoreRepository.deleteById(kid);
	}

	protected String getRefreshToken(String jwsString) {
		Jws<Claims> claims = getClaims(jwsString);
			
		Claims body = claims.getBody();
		String keyId = claims.getHeader().getKeyId();
		UUID kid = UUID.fromString(keyId);

		AuthorizationKeyStore keyStore = authorizationKeyStoreRepository.getOne(kid);
		String username = body.getSubject();

		return buildToken(keyStore, username);
	}

	protected ResponseEntity<Map<String, Object>> getTokenResponse(String token) {
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("token_type", "bearer");
		body.put("access_token", token);
		body.put("expires_in", EXPIRE_SECONDS);

    ResponseCookie tokencookie = buildTokenCookie(token, EXPIRE_SECONDS);
    ResponseCookie isAuthenticatedCookie = buildIsAuthenticatedCookie("true", EXPIRE_SECONDS);
		
		return ResponseEntity.ok()
        .header(HttpHeaders.SET_COOKIE, tokencookie.toString(), isAuthenticatedCookie.toString())
				.body(body);
	}

	protected ResponseCookie buildTokenCookie(String token, int maxAge) {
		return ResponseCookie.from("token", token)
			.maxAge(maxAge)
			.secure(true)
			.httpOnly(true)
			.path("/")
			.sameSite("Strict")
			.build();
	}

	protected ResponseCookie buildIsAuthenticatedCookie(String isAuthenticated, int maxAge) {
		return ResponseCookie.from("isAuthenticated", isAuthenticated)
			.maxAge(maxAge)
			.secure(true)
			.path("/")
			.sameSite("Strict")
			.build();
  }
  
  protected String getResetPasswordToken(String email) {
    
    SecureRandom random = new SecureRandom();
    String token = new String();

    for(int i = 0; i < 16; i++){
      token += random.nextInt(10);
    }

    String encodedToken = bCryptPasswordEncoder.encode(token);

    PasswordReset passwordReset = passwordResetRepository.findOneByEmail(email);

    if(passwordReset == null) {
      passwordReset = new PasswordReset(email, encodedToken);
    }
    else {
      passwordReset.setToken(encodedToken);
    }

    passwordResetRepository.save(passwordReset);
    
    return token;
  }

  protected ResponseEntity<String> sendPasswordResetEmail(String email, String token) {
    
		HttpHeaders reqHeaders = new HttpHeaders();
    reqHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
    Map<String, String> body = new HashMap<String, String>();
    body.put("email", email);
    body.put("token", token);

    HttpEntity<Map<String, String>> requestEntity = 
      new HttpEntity<Map<String, String>>(body, reqHeaders);
  
    //TODO:  Update to use configurable path, or same path as existing service
		ResponseEntity<String> responseEntity = 
			restTemplate().exchange("http://taledir-test.com/email/sendPasswordReset", 
      HttpMethod.POST, requestEntity, String.class);

		return responseEntity;
  }

  protected Map<String, Object> tokenPasswordReset(ResetPasswordDto resetPasswordDto) {

    LoginBadRequestException loginBadRequestException = new LoginBadRequestException(
      "The token you entered is not valid. " +
      "Please ensure you entered it correctly from your email. " +
      "If the sent email is older than 30 minutes, then please send a new email.");

    PasswordReset passwordReset = passwordResetRepository.findOneByEmail(resetPasswordDto.getEmail());

    if(passwordReset == null) {
      throw loginBadRequestException;
    }

    boolean matches = bCryptPasswordEncoder.matches(
			resetPasswordDto.getToken(), passwordReset.getToken());

		if(!matches) {
			throw loginBadRequestException;
		}
    
		Account account = accountRepository.findOneByEmail(resetPasswordDto.getEmail());

    //this code should never happen, unless they delete their account after resetting password
		if(account == null) {
			throw new LoginBadRequestException("There is issue with account, please contact an administrator");
		}

		String encodedPassword = bCryptPasswordEncoder.encode(resetPasswordDto.getPassword());

		account.setPassword(encodedPassword);

    accountRepository.save(account);
    
    passwordResetRepository.delete(passwordReset);

    authorizationKeyStoreRepository.deleteByUsername(account.getUsername());

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("success", true);

		return body;
  }
}