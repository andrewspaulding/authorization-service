package com.taledir.microservices;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class ResetPasswordDto {

  @Email(message = "Please enter a valid email address")
  @NotBlank(message = "Email must be specified")
  private String email;

  String token;

	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,16}$",
		message = "Password must be 8-16 characters, " +
			"contain one lowercase letter, one uppercase letter, one number, " +
			"one special character, and no whitespace characters.")
	private String password;

	public ResetPasswordDto(String email, String token, String password) {
    this.email = email;
    this.token = token;
    this.password = password;
	}

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
  
}