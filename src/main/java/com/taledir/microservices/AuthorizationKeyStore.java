package com.taledir.microservices;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "authorization_key_store")
public class AuthorizationKeyStore {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private UUID id;

	@Column(name = "username", nullable = false)
	private String username;

	@Column(name = "pub_key", length = 294)
  private byte[] pubKey;
  
	@Column(name = "prev_id")
  private UUID prevId;

  @Column(name = "next_id")
  private UUID nextId;

	protected AuthorizationKeyStore() {

	}

  protected AuthorizationKeyStore(String username) {
		super();
		this.username = username;
	}


  public AuthorizationKeyStore(String username, byte[] pubKey, 
      byte[] prevPubKey, UUID prevId, UUID nextId) {
		super();
		this.username = username;
    this.pubKey = pubKey;
    this.prevId = prevId;
    this.nextId = nextId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public byte[] getPubKey() {
		return pubKey;
	}

	public void setPubKey(byte[] pubKey) {
		this.pubKey = pubKey;
	}

  public UUID getPrevId() {
    return prevId;
  }

  public void setPrevId(UUID prevId) {
    this.prevId = prevId;
  }

  public UUID getNextId() {
    return nextId;
  }

  public void setNextId(UUID nextId) {
    this.nextId = nextId;
  }

}
