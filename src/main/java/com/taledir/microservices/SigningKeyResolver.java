package com.taledir.microservices;

import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SigningKeyResolverAdapter;

@Component
public class SigningKeyResolver extends SigningKeyResolverAdapter {

	@Autowired
	AuthorizationKeyStoreRepository authorizationKeyStoreRepository;

    @Override
    @SuppressWarnings("rawtypes") 
    public Key resolveSigningKey(JwsHeader jwsHeader, Claims claims) {

        String keyId = jwsHeader.getKeyId();

        Key key = lookupVerificationKey(keyId);

        return key;
    }

    private Key lookupVerificationKey(String keyId) {
        try {
            UUID kid = UUID.fromString(keyId);
            AuthorizationKeyStore keyStore = authorizationKeyStoreRepository.getOne(kid);
            byte[] pubKeyBytes = keyStore.getPubKey();
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey publicKey = kf.generatePublic(new X509EncodedKeySpec(pubKeyBytes));
            return publicKey;

        } catch (NoSuchAlgorithmException e) {
            throw new LoginBadRequestException("Invalid login session");
        } catch (InvalidKeySpecException e) {
            throw new LoginBadRequestException("Invalid login session");
        } catch (EntityNotFoundException e) {
            throw new LoginBadRequestException("Invalid login session");
        }
    }
}