package com.taledir.microservices;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface PasswordResetRepository extends JpaRepository<PasswordReset, Integer>{

  PasswordReset findOneByEmail(String email);

  @Modifying
  @Transactional
  @Query("delete from PasswordReset p where p.createTimestamp < :expiredTime")
  int deleteByCreateTimestampBefore(Date expiredTime);

}