package com.taledir.microservices;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Integer>{

    Account findOneByUsername(String username);
    
    Account findOneByEmail(String email);
}