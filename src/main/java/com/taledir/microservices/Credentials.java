package com.taledir.microservices;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

public class Credentials {

	private String username;

  @Email(message = "Please enter a valid email address")
	private String email;

	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,16}$",
		message = "Password must be 8-16 characters, " +
			"contain one lowercase letter, one uppercase letter, one number, " +
			"one special character, and no whitespace characters.")
	private String password;

	public Credentials(String username, String email, String password) {
    this.username = username;
    this.email = email;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
